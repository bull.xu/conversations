import 'package:flutter/services.dart';
import 'package:twilio_conversations/api.dart';
import 'package:twilio_conversations/twilio_conversations.dart';

class Participant {
  final String sid;
  final String conversationSid;
  final ChannelType channelType;

  Attributes? _attributes;

  Attributes? get attributes => _attributes;

  String? _dateCreated;

  String? get dateCreated => _dateCreated;

  String? _dateUpdated;

  String? get dateUpdated => _dateUpdated;

  String? _identity;

  String? get identity => _identity;

  int? _lastReadMessageIndex;

  int? get lastReadMessageIndex => _lastReadMessageIndex;

  String? _lastReadTimestamp;

  String? get lastReadTimestamp => _lastReadTimestamp;

  Participant(
    this.sid,
    this.channelType,
    this.conversationSid,
    this._attributes,
    this._dateCreated,
    this._dateUpdated,
    this._identity,
    this._lastReadMessageIndex,
    this._lastReadTimestamp,
  );

  factory Participant.fromPigeon(ParticipantData participantData) {
    return Participant(
      participantData.sid ?? '',
      ChannelType(participantData.channelType ?? ''),
      participantData.conversationSid ?? '',
      participantData.attributes?.toAttributes(),
      participantData.dateCreated,
      participantData.dateUpdated,
      participantData.identity,
      participantData.lastReadMessageIndex,
      participantData.lastReadTimestamp,
    );
  }

  Future<User?> getUser() async {
    try {
      final result = await TwilioConversations()
          .participantApi
          .getUser(conversationSid, sid);

      return User.fromPigeon(result);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  Future<Conversation?> getConversation() async {
    return TwilioConversations.conversationClient!
        .getConversation(conversationSid);
  }

  Future<void> setAttributes(Attributes attributes) async {
    try {
      final attributesData = AttributesData()
        ..type = attributes.type.name
        ..data = attributes.data;
      await TwilioConversations()
          .participantApi
          .setAttributes(conversationSid, sid, attributesData);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  Future<void> remove() async {
    try {
      await TwilioConversations().participantApi.remove(conversationSid, sid);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }
}
