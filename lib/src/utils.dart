extension StringToDate on String {
  DateTime? toDate() => DateTime.tryParse(this);
}
