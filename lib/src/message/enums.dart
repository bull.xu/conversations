// ignore_for_file: constant_identifier_names

/// Indicates reason for message update.
enum MessageUpdateReason {
  /// [Message] body has been updated.
  BODY,

  /// [Message] attributes have been updated.
  ATTRIBUTES,

  /// [Message] aggregated delivery receipt has been updated.
  DELIVERY_RECEIPT,

  /// The message’s subject changed.
  SUBJECT,
}

enum DeliveryAmount {
  /// The amount for the delivery statuses is 0.
  NONE,

  /// Amount of the delivery statuses is at least 1.
  SOME,

  /// Amount of the delivery statuses equals the maximum number of delivery events expected for that message.
  ALL,
}
