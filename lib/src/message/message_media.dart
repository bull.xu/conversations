import 'package:twilio_conversations/api.dart';

class MessageMedia {
  final String _sid;
  final String? _filename;
  final String? _contentType;
  final int _size;
  final String? _category;

  const MessageMedia._(
    this._sid,
    this._filename,
    this._contentType,
    this._size,
    this._category,
  );

  //#region Public API properties
  /// Get SID of media stream.
  String get sid {
    return _sid;
  }

  /// Get file name of media stream.
  String? get filename {
    return _filename;
  }

  /// Get mime-type of media stream.
  String? get contentType {
    return _contentType;
  }

  /// Get size of media stream.
  int get size {
    return _size;
  }

  String? get category {
    return _category;
  }

  //#endregion

  /// Construct from a map.
  factory MessageMedia.fromPigeon(MediaData data) {
    return MessageMedia._(
      data.sid ?? '',
      data.filename,
      data.contentType,
      data.size ?? 0,
      data.category,
    );
  }
}
