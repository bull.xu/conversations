import 'package:collection/collection.dart';
import 'package:flutter/services.dart';
import 'package:twilio_conversations/api.dart';
import 'package:twilio_conversations/src/utils.dart';
import 'package:twilio_conversations/twilio_conversations.dart';

class Message {
  final String? sid;
  final int? index;
  final String? author;
  final String? subject;
  final String? body;
  final List<MessageMedia>? attachedMedia;
  final String conversationSid;
  final String? participantSid;
  final DateTime? dateCreated;
  final DateTime? dateUpdated;
  final String? lastUpdatedBy;
  final Attributes? attributes;

  Message(
    this.sid,
    this.index,
    this.author,
    this.subject,
    this.body,
    this.attachedMedia,
    this.conversationSid,
    this.participantSid,
    this.dateCreated,
    this.dateUpdated,
    this.lastUpdatedBy,
    this.attributes,
  );

  factory Message.fromPigeon(MessageData messageData) {
    return Message(
      messageData.sid,
      messageData.index,
      messageData.author,
      messageData.subject,
      messageData.body,
      messageData.attachedMedia
          ?.whereNotNull()
          .map((m) => MessageMedia.fromPigeon(m))
          .toList(),
      messageData.conversationSid!,
      messageData.participantSid,
      messageData.dateCreated?.toDate(),
      messageData.dateUpdated?.toDate(),
      messageData.lastUpdatedBy,
      messageData.attributes?.toAttributes(),
    );
  }

  Future<Conversation?> getConversation() async {
    try {
      return TwilioConversations.conversationClient
          ?.getConversation(conversationSid);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  /// Save media content stream that could be streamed or downloaded by client.
  ///
  /// Provided file could be an existing file and a none existing file.
  Future<Map<String?, String?>?> getMediaUrls() async {
    try {
      final uMessageIndex = index;
      if (uMessageIndex != null) {
        final result = await TwilioConversations()
            .messageApi
            .getTemporaryContentUrlsForMedia(conversationSid, uMessageIndex);
        return result;
      }
      return null;
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  Future<Participant?> getParticipant() async {
    try {
      final result = await TwilioConversations()
          .messageApi
          .getParticipant(conversationSid, index!);

      final participant = Participant.fromPigeon(result);

      return participant;
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  Future<void> setAttributes(Attributes attributes) async {
    try {
      final attributesData = AttributesData()
        ..type = attributes.type.name
        ..data = attributes.data;
      await TwilioConversations()
          .messageApi
          .setAttributes(conversationSid, index!, attributesData);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }

  Future<void> updateMessageBody(String messageBody) async {
    try {
      await TwilioConversations()
          .messageApi
          .updateMessageBody(conversationSid, index!, messageBody);
    } on PlatformException catch (err) {
      throw TwilioConversations.convertException(err);
    }
  }
//TODO: implement getAggregatedDeliveryReceipt
//TODO: implement getDetailedDeliveryReceiptList
}
