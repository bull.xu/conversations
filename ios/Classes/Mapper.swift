import Flutter
import SwiftyJSON
import TwilioConversationsClient

enum Mapper {
    static let TAG = "Mapper"

    public static func conversationsClientToPigeon(
        _ client: TwilioConversationsClient?) -> ConversationClientData? {
        guard let client = client else {
            return nil
        }
        var result = ConversationClientData()
        result.myIdentity = client.user?.identity
        result.connectionState = clientConnectionStateToString(client.connectionState)
        result.isReachabilityEnabled = client.isReachabilityEnabled()
        return result
    }

    public static func conversationsList(_ conversations: [TCHConversation]?) -> [ConversationData]? {
        return conversations?.compactMap { conversationToPigeon($0) }
    }

    public static func conversationToPigeon(_ conversation: TCHConversation?) -> ConversationData? {
        guard let conversation = conversation,
              let sid = conversation.sid else {
            return nil
        }

        if !SwiftTwilioConversationsPlugin.conversationListeners.keys.contains(sid) {
            debug("setupConversationListener => conversation: \(String(describing: conversation.sid))")
            SwiftTwilioConversationsPlugin.conversationListeners[sid] = ConversationListener(sid)
            conversation.delegate = SwiftTwilioConversationsPlugin.conversationListeners[sid]
        }

        var result = ConversationData()
        result.attributes = attributesToPigeon(conversation.attributes())
        result.createdBy = conversation.createdBy
        result.dateCreated = dateToString(conversation.dateCreatedAsDate)
        result.dateUpdated = dateToString(conversation.dateUpdatedAsDate)
        result.friendlyName = conversation.friendlyName
        result.lastMessageDate = dateToString(conversation.lastMessageDate)
        result.lastMessageIndex = conversation.lastMessageIndex?.int64Value
        result.lastReadMessageIndex = conversation.lastReadMessageIndex?.int64Value
        result.sid = sid
        result.status = conversationStatusToString(conversation.status)
        result.synchronizationStatus = conversationSynchronizationStatusToString(conversation.synchronizationStatus)
        result.uniqueName = conversation.uniqueName
        return result
    }

    public static func messageToPigeon(_ message: TCHMessage, conversationSid: String?) -> MessageData {
        var result = MessageData()
        result.sid = message.sid
        result.author = message.author
        result.dateCreated = message.dateCreated
        result.dateUpdated = message.dateUpdated
        result.lastUpdatedBy = message.lastUpdatedBy
        result.body = message.body
        result.conversationSid = conversationSid
        result.participantSid = message.participantSid
        result.index = message.index?.int64Value
        result.attachedMedia = message.attachedMedia.map(mediaToPigeon)
        result.attributes = attributesToPigeon(message.attributes())
        return result
    }

    public static func participantToPigeon(
        _ participant: TCHParticipant?,
        conversationSid: String?) -> ParticipantData? {
        guard let participant = participant else {
            return nil
        }

        var result = ParticipantData()
        result.sid = participant.sid
        result.conversationSid = conversationSid
        result.lastReadMessageIndex = participant.lastReadMessageIndex?.int64Value
        result.lastReadTimestamp = participant.lastReadTimestamp
        result.dateCreated = participant.dateCreated
        result.dateUpdated = participant.dateUpdated
        result.identity = participant.identity
        result.channelType = participantChannelTypeToString(participant.channelType)
        result.attributes = attributesToPigeon(participant.attributes())
        return result
    }

    public static func userToPigeon(_ user: TCHUser?) -> UserData? {
        guard let user = user else {
            return nil
        }
        var result = UserData()
        result.friendlyName = user.friendlyName
        result.attributes = attributesToPigeon(user.attributes())
        result.identity = user.identity
        result.isOnline = user.isOnline()
        result.isNotifiable = user.isNotifiable()
        result.isSubscribed = user.isSubscribed()
        return result
    }

    public static func mediaToPigeon(_ media: Media) -> MediaData {
        var result = MediaData()
        result.sid = media.sid
        result.filename = media.filename
        result.contentType = media.contentType
        result.size = Int64(media.size)
        result.category = mediaCategoryTypeToString(media.category)
        return result
    }

    public static func attributesToPigeon(_ attributes: TCHJsonAttributes?) -> AttributesData? {
        var result = AttributesData()
        result.type = "NULL"
        result.data = nil

        if let attr = attributes {
            if attr.isNumber {
                result.type = "NUMBER"
                result.data = attr.number?.stringValue
            } else if attr.isArray {
                let jsonData = JSON(attr.array ?? [])
                result.type = "ARRAY"
                if #available(iOS 13.0, *) {
                    result.data = jsonData.rawString(options: .withoutEscapingSlashes)
                } else {
                    // Fallback on earlier versions
                    result.data = jsonData.rawString()
                }
            } else if attr.isString {
                result.type = "STRING"
                result.data = attr.string
            } else if attr.isDictionary {
                let jsonData = JSON(attr.dictionary as Any)
                result.type = "OBJECT"
                if #available(iOS 13.0, *) {
                    result.data = jsonData.rawString(options: .withoutEscapingSlashes)
                } else {
                    // Fallback on earlier versions
                    result.data = jsonData.rawString()
                }
            }
        }
        debug("attributesToPigeon => \(String(describing: result.data))")
        return result
    }

    public static func pigeonToAttributes(_ attributesData: AttributesData) throws -> TCHJsonAttributes? {
        var result: TCHJsonAttributes?
        do {
            switch attributesData.type {
            case "NULL":
                result = nil
            case "NUMBER":
                let number = NumberFormatter().number(from: attributesData.data!)
                result = TCHJsonAttributes(number: number!)
            case "ARRAY":
                guard let objectData = attributesData.data!.data(using: .utf8),
                      let array = try JSON(data: objectData).arrayObject else {
                    throw LocalizedConversionError.invalidData
                }

                result = TCHJsonAttributes(array: array)
            case "STRING":
                result = TCHJsonAttributes(string: attributesData.data!)
            case "OBJECT":
                guard let objectData = attributesData.data!.data(using: .utf8),
                      let object = try JSON(data: objectData).dictionaryObject else {
                    throw LocalizedConversionError.invalidData
                }

                result = TCHJsonAttributes(dictionary: object)
            default:
                throw LocalizedConversionError.invalidType
            }
        } catch let error {
            debug("pigeonToAttributes => ERROR \(error)")
            return nil
        }
        debug("pigeonToAttributes => \(String(describing: result))")
        return result
    }

    public static func stringToNotificationLevel(_ level: String) -> TCHConversationNotificationLevel? {
        switch level {
        case "DEFAULT":
            return TCHConversationNotificationLevel.default
        case "MUTED":
            return TCHConversationNotificationLevel.muted
        default:
            return nil
        }
    }

    public static func errorToPigeon(_ error: TCHError) -> ErrorInfoData {
        var errorInfoData = ErrorInfoData()
        errorInfoData.code = Int64(error.code)
        errorInfoData.message = error.description
        return errorInfoData
    }

    public static func conversationStatusToString(_ conversationStatus: TCHConversationStatus) -> String {
        let conversationStatusString: String

        switch conversationStatus {
        case .joined:
            conversationStatusString = "JOINED"
        case .notParticipating:
            conversationStatusString = "NOT_PARTICIPATING"
        @unknown default:
            conversationStatusString = "UNKNOWN"
        }

        return conversationStatusString
    }

    public static func clientConnectionStateToString(_ connectionState: TCHClientConnectionState?) -> String {
        var connectionStateString: String = "UNKNOWN"
        if let connectionState = connectionState {
            switch connectionState {
            case .unknown:
                connectionStateString = "UNKNOWN"
            case .disconnected:
                connectionStateString = "DISCONNECTED"
            case .connected:
                connectionStateString = "CONNECTED"
            case .connecting:
                connectionStateString = "CONNECTING"
            case .denied:
                connectionStateString = "DENIED"
            case .error:
                connectionStateString = "ERROR"
            case .fatalError:
                connectionStateString = "FATAL_ERROR"
            default:
                connectionStateString = "UNKNOWN"
            }
        }

        return connectionStateString
    }

    public static func clientSynchronizationStatusToString(_ syncStatus: TCHClientSynchronizationStatus?) -> String {
        var syncStateString: String = "UNKNOWN"
        if let syncStatus = syncStatus {
            switch syncStatus {
            case .started:
                syncStateString = "STARTED"
            case .completed:
                syncStateString = "COMPLETED"
            case .conversationsListCompleted:
                syncStateString = "CONVERSATIONS_LIST_COMPLETED"
            case .failed:
                syncStateString = "FAILED"
            @unknown default:
                syncStateString = "UNKNOWN"
            }
        }

        return syncStateString
    }

    public static func conversationSynchronizationStatusToString(
        _ syncStatus: TCHConversationSynchronizationStatus) -> String {
        let syncStatusString: String

        switch syncStatus {
        case .none:
            syncStatusString = "NONE"
        case .identifier:
            syncStatusString = "IDENTIFIER"
        case .metadata:
            syncStatusString = "METADATA"
        case .all:
            syncStatusString = "ALL"
        case .failed:
            syncStatusString = "FAILED"
        @unknown default:
            syncStatusString = "UNKNOWN"
        }

        return syncStatusString
    }

    public static func conversationUpdateToString(_ update: TCHConversationUpdate) -> String {
        switch update {
        case .attributes:
            return "ATTRIBUTES"
        case .friendlyName:
            return "FRIENDLY_NAME"
        case .lastMessage:
            return "LAST_MESSAGE"
        case .lastReadMessageIndex:
            return "LAST_READ_MESSAGE_INDEX"
        case .state:
            return "STATE"
        case .status:
            return "STATUS"
        case .uniqueName:
            return "UNIQUE_NAME"
        case .userNotificationLevel:
            return "NOTIFICATION_LEVEL"
        @unknown default:
            return "UNKNOWN"
        }
    }

    public static func participantChannelTypeToString(_ participantType: TCHParticipant.ParticipantChannelType?) -> String? {
        switch participantType {
        case nil:
            return nil;
        case .chat:
            return "CHAT"
        case .sms:
            return "SMS"
        case .other(let other):
            return other
        case .whatsapp:
            return "WHATSAPP"
        }
    }

    public static func mediaCategoryTypeToString(_ mediaCategory: MediaCategory) -> String {
        switch mediaCategory {
        case .media:
            return "MEDIA"
        case .body:
            return "BODY"
        case .history:
            return "HISTORY"
        @unknown default:
            return "UNKNOWN"
        }
    }

    public static func messageUpdateToString(_ update: TCHMessageUpdate) -> String {
        let updateString: String

        switch update {
        case .attributes:
            updateString = "ATTRIBUTES"
        case .body:
            updateString = "BODY"
        case .deliveryReceipt:
            updateString = "DELIVERY_RECEIPT"
        case .subject:
            updateString = "SUBJECT"
        @unknown default:
            updateString = "UNKNOWN"
        }

        return updateString
    }

    public static func participantUpdateToString(_ update: TCHParticipantUpdate) -> String {
        let updateString: String

        switch update {
        case .attributes:
            updateString = "ATTRIBUTES"
        case .lastReadMessageIndex:
            updateString = "LAST_READ_MESSAGE_INDEX"
        case .lastReadTimestamp:
            updateString = "LAST_READ_TIMESTAMP"
        @unknown default:
            updateString = "UNKNOWN"
        }

        return updateString
    }

    public static func userUpdateToString(_ update: TCHUserUpdate) -> String {
        switch update {
        case .friendlyName:
            return "FRIENDLY_NAME"
        case .attributes:
            return "ATTRIBUTES"
        case .reachabilityOnline:
            return "REACHABILITY_ONLINE"
        case .reachabilityNotifiable:
            return "REACHABILITY_NOTIFIABLE"
        @unknown default:
            return "UNKNOWN"
        }
    }

    public static func dateToString(_ date: Date?) -> String? {
        if let date = date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            return formatter.string(from: date)
        }
        return nil
    }

    private static func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}

enum LocalizedConversionError: LocalizedError {
    case invalidType
    case invalidData
}
