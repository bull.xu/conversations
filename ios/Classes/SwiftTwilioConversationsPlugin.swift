import Flutter
import UIKit
import TwilioConversationsClient

public class SwiftTwilioConversationsPlugin: NSObject, FlutterPlugin {
    let TAG = "SwiftTwilioConversationsPlugin"

    public static var instance: SwiftTwilioConversationsPlugin?

    // Flutter > Host APIs
    static let pluginApi: PluginMethods = PluginMethods()
    static let conversationClientApi: ConversationClientMethods = ConversationClientMethods()
    static let conversationApi: ConversationMethods = ConversationMethods()
    static let participantApi: ParticipantMethods = ParticipantMethods()
    static let messageApi: MessageMethods = MessageMethods()
    static let userApi: UserMethods = UserMethods()

    // Host > Flutter APIs
    static var flutterClientApi: FlutterConversationClientApi?
    static var flutterLoggingApi: FlutterLoggingApi?

    public var client: TwilioConversationsClient?

    public static var clientListener: ClientListener?
    public static var conversationListeners: [String: ConversationListener] = [:]

    public static var messenger: FlutterBinaryMessenger?

    public static var reasonForTokenRetrieval: String?

    public static var nativeDebug = false

    public static func debug(_ msg: String) {
        if SwiftTwilioConversationsPlugin.nativeDebug {
            NSLog(msg)
            guard let loggingApi = SwiftTwilioConversationsPlugin.flutterLoggingApi else {
                return
            }
            loggingApi.logFromHost(msg: msg) {  }
        }
    }

    public static func register(with registrar: FlutterPluginRegistrar) {
        instance = SwiftTwilioConversationsPlugin()
        instance?.onRegister(registrar)
    }

    public func onRegister(_ registrar: FlutterPluginRegistrar) {
        SwiftTwilioConversationsPlugin.messenger = registrar.messenger()

        SwiftTwilioConversationsPlugin.flutterClientApi =
            FlutterConversationClientApi(binaryMessenger: registrar.messenger())
        SwiftTwilioConversationsPlugin.flutterLoggingApi =
            FlutterLoggingApi(binaryMessenger: registrar.messenger())

        PluginApiSetup.setUp(binaryMessenger: registrar.messenger(), api: SwiftTwilioConversationsPlugin.pluginApi)
        ConversationClientApiSetup.setUp(binaryMessenger: registrar.messenger(), api: SwiftTwilioConversationsPlugin.conversationClientApi)
        ConversationApiSetup.setUp(binaryMessenger: registrar.messenger(), api: SwiftTwilioConversationsPlugin.conversationApi)
        ParticipantApiSetup.setUp(binaryMessenger: registrar.messenger(), api: SwiftTwilioConversationsPlugin.participantApi)
        MessageApiSetup.setUp(binaryMessenger: registrar.messenger(), api:  SwiftTwilioConversationsPlugin.messageApi)
        UserApiSetup.setUp(binaryMessenger: registrar.messenger(), api: SwiftTwilioConversationsPlugin.userApi)

        registrar.addApplicationDelegate(self)
    }

    // swiftlint:disable function_body_length cyclomatic_complexity
    public func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        debug("didRegisterForRemoteNotificationsWithDeviceToken => onSuccess: \((deviceToken as NSData).description)")
        if let reason = SwiftTwilioConversationsPlugin.reasonForTokenRetrieval {
            if reason == "register" {
                client?.register(withNotificationToken: deviceToken, completion: { (result: TCHResult) in
                    self.debug("didRegisterForRemoteNotificationsWithDeviceToken => "
                               + "registered for notifications: \(result.isSuccessful)")
                    if result.isSuccessful {
                        SwiftTwilioConversationsPlugin.flutterClientApi?.registered() { }
                    } else if let error = result.error {
                        SwiftTwilioConversationsPlugin.flutterClientApi?.registrationFailed(errorInfoData: Mapper.errorToPigeon(error)) { }
                    } else {
                        var error = ErrorInfoData()
                        error.code = 0
                        error.message = "Unknown error during registration."
                        SwiftTwilioConversationsPlugin.flutterClientApi?.registrationFailed(errorInfoData: error) { }
                    }
                })
            } else {
                client?.deregister(withNotificationToken: deviceToken) { (result: TCHResult) in
                    self.debug("didRegisterForRemoteNotificationsWithDeviceToken => "
                          + "deregistered for notifications: \(result.isSuccessful)")
                    if result.isSuccessful {
                        SwiftTwilioConversationsPlugin.flutterClientApi?.deregistered() { }
                    } else if let error = result.error {
                        SwiftTwilioConversationsPlugin.flutterClientApi?.registrationFailed(
                            errorInfoData: Mapper.errorToPigeon(error)
                        ) { }
                    } else {
                        var error = ErrorInfoData()
                        error.code = 0
                        error.message = "Unknown error during deregistration."
                        SwiftTwilioConversationsPlugin.flutterClientApi?.registrationFailed(
                            errorInfoData: error
                        ) { }
                    }
                }
            }
        }
    }

    public func application(_ application: UIApplication,
                            didFailToRegisterForRemoteNotificationsWithError
        error: Error) {
        debug("didFailToRegisterForRemoteNotificationsWithError => onError: \(error)")
        let error = error as NSError
        var exception = ErrorInfoData()
        exception.code = Int64(error.code)
        exception.message = error.localizedDescription
        SwiftTwilioConversationsPlugin.flutterClientApi?.registrationFailed(
            errorInfoData: exception
        ) { }
    }

    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}

extension FlutterError: Error {}
