import TwilioConversationsClient

public class ClientListener: NSObject, TwilioConversationsClientDelegate {
    let TAG = "ClientListener"
    
    var api: FlutterConversationClientApi? {
        get {
            return SwiftTwilioConversationsPlugin.flutterClientApi
        }
    }
    
    // onAddedToConversation Notification
    public func conversationsClient(_ client: TwilioConversationsClient, notificationAddedToConversationWithSid conversationSid: String) {
        debug("onAddedToConversationNotification => conversationSid is \(conversationSid)'")
        api?.addedToConversationNotification(conversationSid: conversationSid) { }
    }
    
    // onClientSynchronizationUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        let statusString = Mapper.clientSynchronizationStatusToString(status)
        debug("onClientSynchronization => state is \(statusString)")
        api?.clientSynchronization(synchronizationStatus: statusString) { }
    }
    
    // onConnectionStateChange
    public func conversationsClient(_ client: TwilioConversationsClient, connectionStateUpdated state: TCHClientConnectionState) {
        let stateString = Mapper.clientConnectionStateToString(state)
        debug("onConnectionStateChange => state is \(stateString)")
        api?.connectionStateChange(connectionState: stateString) { }
    }
    
    // onConversationAdded
    public func conversationsClient(_ client: TwilioConversationsClient, conversationAdded conversation: TCHConversation) {
        debug("onConversationAdded => conversation \(String(describing: conversation.sid))'")
        api?.conversationAdded(conversationData: Mapper.conversationToPigeon(conversation)!) { }
    }
    
    // onConversationDeleted
    public func conversationsClient(_ client: TwilioConversationsClient, conversationDeleted conversation: TCHConversation) {
        debug("onConversationDeleted => conversation \(String(describing: conversation.sid))'")
        api?.conversationDeleted(conversationData: Mapper.conversationToPigeon(conversation)!) { }
    }
    
    // onConversationSynchronizationChanged
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, synchronizationStatusUpdated status: TCHConversationSynchronizationStatus) {
        debug("onConversationSynchronizationChange => conversationSid is '\(String(describing: conversation.sid))', "
              + "syncStatus: \(Mapper.conversationSynchronizationStatusToString(conversation.synchronizationStatus))")
        api?.conversationSynchronizationChange(conversationData: Mapper.conversationToPigeon(conversation)!) { }
    }
    
    // onConversationUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, updated: TCHConversationUpdate) {
        debug("conversationUpdated => conversation "
              + "\(String(describing: conversation.sid)) updated, \(Mapper.conversationUpdateToString(updated))")
        
        var event = ConversationUpdatedData()
        event.conversation = Mapper.conversationToPigeon(conversation)
        event.reason = Mapper.conversationUpdateToString(updated)
        api?.conversationUpdated(event: event) { }
    }
    
    // onError
    public func conversationsClient(_ client: TwilioConversationsClient, errorReceived error: TCHError) {
        debug("onError")
        api?.error(errorInfoData: Mapper.errorToPigeon(error)) { }
    }
    
    // onNewMessageNotification
    public func conversationsClient(_ client: TwilioConversationsClient, notificationNewMessageReceivedForConversationSid conversationSid: String, messageIndex: UInt) {
        debug("onNewMessageNotification => conversationSid: \(conversationSid), messageIndex: \(messageIndex)")
        api?.newMessageNotification(conversationSid: conversationSid, messageIndex: Int64(messageIndex)) { }
    }
    
    // onRemovedFromConversationNotification
    public func conversationsClient(_ client: TwilioConversationsClient, notificationRemovedFromConversationWithSid conversationSid: String) {
        debug("onRemovedFromConversationNotification => conversationSid: \(conversationSid)")
        api?.removedFromConversationNotification(conversationSid: conversationSid) { }
    }
    
    // onTokenExpired
    public func conversationsClientTokenExpired(_ client: TwilioConversationsClient) {
        debug("onTokenExpired")
        api?.tokenExpired() { }
    }
    
    // onTokenAboutToExpire
    public func conversationsClientTokenWillExpire(_ client: TwilioConversationsClient) {
        debug("onTokenAboutToExpire")
        api?.tokenAboutToExpire() { }
    }
    
    // onUserSubscribed
    public func conversationsClient(_ client: TwilioConversationsClient, userSubscribed user: TCHUser) {
        debug("onUserSubscribed => user '\(String(describing: user.identity))'")
        api?.userSubscribed(userData: Mapper.userToPigeon(user)!) { }
    }
    
    // onUserUnsubscribed
    public func conversationsClient(_ client: TwilioConversationsClient, userUnsubscribed user: TCHUser) {
        debug("onUserUnsubscribed => user '\(String(describing: user.identity))'")
        api?.userUnsubscribed(
            userData: Mapper.userToPigeon(user)!
        ) { }
    }
    
    // onUserUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, user: TCHUser, updated: TCHUserUpdate) {
        debug("onUserUpdated => user \(String(describing: user.identity)) "
              + "updated, \(Mapper.userUpdateToString(updated))")
        api?.userUpdated(
            userData: Mapper.userToPigeon(user)!,
            reason: Mapper.userUpdateToString(updated)
        ) { }
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
