import Flutter
import TwilioConversationsClient

public class ConversationListener: NSObject, TCHConversationDelegate {
    let TAG = "ConversationListener"
    let conversationSid: String

    var api: FlutterConversationClientApi? {
        get {
            return SwiftTwilioConversationsPlugin.flutterClientApi
        }
    }
    
    init(_ conversationSid: String) {
        self.conversationSid = conversationSid
    }

    // onMessageAdded
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, messageAdded message: TCHMessage) {
        debug("onMessageAdded => messageSid = \(String(describing: message.sid))")
        api?.messageAdded(
            conversationSid: conversationSid,
            messageData: Mapper.messageToPigeon(message, conversationSid: conversationSid)
        ) { }
    }

    // onMessageUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, message: TCHMessage, updated: TCHMessageUpdate) {
        debug("onMessageUpdated => messageSid = \(String(describing: message.sid)), " +
                "updated = \(String(describing: updated))")
        api?.messageUpdated(
            conversationSid: conversationSid,
            messageData: Mapper.messageToPigeon(message, conversationSid: conversationSid),
            reason: Mapper.messageUpdateToString(updated)
        ) { }
    }

    // onMessageDeleted
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, messageDeleted message: TCHMessage) {
        debug("onMessageDeleted => messageSid = \(String(describing: message.sid))")
        api?.messageDeleted(
            conversationSid: conversationSid,
            messageData: Mapper.messageToPigeon(message, conversationSid: conversationSid)
        ) { }
    }

    // onParticipantAdded
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participantJoined participant: TCHParticipant) {
        debug("onParticipantAdded => participantSid = \(String(describing: participant.sid))")
        api?.participantAdded(
            conversationSid: conversationSid,
            participantData: Mapper.participantToPigeon(participant, conversationSid: conversationSid)!
        ) { }
    }

    // onParticipantUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participant: TCHParticipant, updated: TCHParticipantUpdate) {
        debug("onParticipantUpdated => participantSid = \(String(describing: participant.sid)), " +
                "updated = \(String(describing: updated))")
        api?.participantUpdated(
            conversationSid: conversationSid,
            participantData: Mapper.participantToPigeon(participant, conversationSid: conversationSid)!,
            reason: Mapper.participantUpdateToString(updated)
        ) { }
    }

    // onParticipantDeleted
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participantLeft participant: TCHParticipant) {
        debug("onParticipantDeleted => participantSid = \(String(describing: participant.sid))")
        api?.participantDeleted(
            conversationSid: conversationSid,
            participantData: Mapper.participantToPigeon(participant, conversationSid: conversationSid)!
        ) { }
    }

    // onTypingStarted
    public func conversationsClient(_ client: TwilioConversationsClient, typingStartedOn conversation: TCHConversation, participant: TCHParticipant) {
        debug("onTypingStarted => conversationSid = \(String(describing: conversation.sid)), " +
                "participantSid = \(String(describing: participant.sid))")
        api?.typingStarted(
            conversationSid: conversationSid,
            conversationData: Mapper.conversationToPigeon(conversation)!,
            participantData: Mapper.participantToPigeon(participant, conversationSid: conversationSid)!
        ) { }
    }

    // onTypingEnded
    public func conversationsClient(_ client: TwilioConversationsClient, typingEndedOn conversation: TCHConversation, participant: TCHParticipant) {
        debug("onTypingEnded => conversationSid = \(String(describing: conversation.sid)), " +
              "participantSid = \(String(describing: participant.sid))")
        api?.typingEnded(
            conversationSid: conversationSid,
            conversationData: Mapper.conversationToPigeon(conversation)!,
            participantData: Mapper.participantToPigeon(participant, conversationSid: conversationSid)!
        ) { }
    }

    // onSynchronizationChanged
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, synchronizationStatusUpdated status: TCHConversationSynchronizationStatus) {
        let syncStatus = Mapper.conversationSynchronizationStatusToString(conversation.synchronizationStatus)
        debug("onSynchronizationChanged => sid: \(String(describing: conversation.sid)), status: \(syncStatus)")
        api?.synchronizationChanged(
            conversationSid: conversationSid,
            conversationData: Mapper.conversationToPigeon(conversation)!
        ) { }
    }

    // The ConversationListener Protocol for iOS duplicates some of the events
    // that are provided via the ClientListener protocol on both Android and iOS.
    // In the interest of functional parity and avoid duplicate notifications,
    // we will not notify the dart layer of such event from the ConversationListener.
    // userSubscribed
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participant: TCHParticipant, userSubscribed user: TCHUser) {
        debug("handled by ClientListener.userSubscribed")
    }

    // userUnsubscribed
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participant: TCHParticipant, userUnsubscribed user: TCHUser) {
        debug("handled by ClientListener.userUnsubscribed")
    }

    // userUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, participant: TCHParticipant, user: TCHUser, updated: TCHUserUpdate) {
        debug("handled by ClientListener.userUpdated")
    }

    // onConversationDeleted
    public func conversationsClient(_ client: TwilioConversationsClient, conversationDeleted conversation: TCHConversation) {
        debug("handled by ClientListener.onConversationDeleted")
    }

    // onConversationUpdated
    public func conversationsClient(_ client: TwilioConversationsClient, conversation: TCHConversation, updated: TCHConversationUpdate) {
        debug("handled by ClientListener.onConversationUpdated")
    }

    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
