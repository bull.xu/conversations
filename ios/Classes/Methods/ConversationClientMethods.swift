import Flutter
import TwilioConversationsClient

class ConversationClientMethods: NSObject, ConversationClientApi {
    let TAG = "ConversationClientMethods"
    
    /// A private method to ensure [TwilioConversationsClient] is ready, and
    /// connect to [conversation] with [conversationSid].
    private func perform<S>(
        _ conversationSid: String,
        completion: @escaping (Result<S, Error>) -> Void,
        action: @escaping (TwilioConversationsClient, TCHConversation) -> Void
    ) {
        
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        
        client.conversation(withSidOrUniqueName: conversationSid) { (result, conversation) in
            
            if result.isSuccessful, let conversation = conversation {
                action(client, conversation)
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("perform => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating conversation with sid or uniqueName \(conversationSid): \(errorMessage)",
                            details: nil)))
            }
        }
    }
    
    /// getConversation
    public func getConversation(conversationSidOrUniqueName: String, completion: @escaping (Result<ConversationData, Error>) -> Void) {
        self.debug("getConversation => conversationSidOrUniqueName: \(String(describing: conversationSidOrUniqueName))")
        perform(conversationSidOrUniqueName, completion: completion) { _, conversation in
            self.debug("getConversation => onSuccess")
            completion(Result.success(Mapper.conversationToPigeon(conversation)!))
        }
    }
    
    /// getMyConversations
    public func getMyConversations(completion: @escaping (Result<[ConversationData], Error>) -> Void) {
        self.debug("getMyConversations")
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        let myConversations =  client.myConversations()
        let result = Mapper.conversationsList(myConversations)
        completion(Result.success(result!))
    }
    
    /// getMyUser
    public func getMyUser(completion: @escaping (Result<UserData, Error>) -> Void) {
        self.debug("getMyUser")
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        let myUser = client.user
        return completion(Result.success(Mapper.userToPigeon(myUser)!))
    }
    
    /// updateToken
    func updateToken(token: String, completion: @escaping (Result<Void, Error>) -> Void) {
        self.debug("updateToken")
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        client.updateToken(token, completion: {(result: TCHResult) -> Void in
            if result.isSuccessful {
                self.debug("updateToken => onSuccess")
                completion(Result.success(()))
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("updateToken => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "TwilioException",
                            message: "\(String(describing: result.error?.code))|Error updating token: \(errorMessage)",
                            details: nil)))
            }
        } as TCHCompletion)
    }
    
    /// shutdown
    public func shutdown() throws {
        self.debug("shutdown")
        SwiftTwilioConversationsPlugin.instance?.client?.shutdown()
        disposeListeners()
    }
    
    /// createConversation
    public func createConversation(friendlyName: String, completion: @escaping (Result<ConversationData, Error>) -> Void) {
        self.debug("createConversation => friendlyName: \(String(describing: friendlyName))")
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        let conversationOptions: [String: Any] = [
            TCHConversationOptionFriendlyName: friendlyName
        ]
        
        client.createConversation(
            options: conversationOptions,
            completion: { (result: TCHResult, conversation: TCHConversation?) in
                if result.isSuccessful, let conversation = conversation {
                    self.debug("createConversation => onSuccess")
                    let conversationDict = Mapper.conversationToPigeon(conversation)
                    completion(Result.success(conversationDict!))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("createConversation => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error creating conversation with "
                                + "friendlyName '\(friendlyName)': \(errorMessage)",
                                details: nil)))
                }
            })
    }
    
    /// registerForNotifications
    public func registerForNotification(tokenData: TokenData, completion: @escaping (Result<Void, Error>) -> Void) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(
                options: [.alert, .sound, .badge]) { (granted: Bool, _: Error?) in
                    self.debug("register => User responded to permissions request: \(granted)")
                    if granted {
                        DispatchQueue.main.async {
                            self.debug("register => Requesting APNS token")
                            SwiftTwilioConversationsPlugin.reasonForTokenRetrieval = "register"
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    }
                }
        }
        completion(Result.success(()))
    }
    
    /// unregisterForNotifications
    public func unregisterForNotification(tokenData: TokenData, completion: @escaping (Result<Void, Error>) -> Void) {
        if #available(iOS 10.0, *) {
            DispatchQueue.main.async {
                self.debug("unregister => Requesting APNS token")
                SwiftTwilioConversationsPlugin.reasonForTokenRetrieval = "deregister"
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        completion(Result.success(()))
    }
    
    private func disposeListeners() {
        SwiftTwilioConversationsPlugin.clientListener = nil
        SwiftTwilioConversationsPlugin.conversationListeners.removeAll()
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
