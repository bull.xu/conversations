import Flutter
import TwilioConversationsClient

class PluginMethods: NSObject, PluginApi {
    let TAG = "PluginMethods"
    
    func debug(enableNative: Bool, enableSdk: Bool) throws {
        SwiftTwilioConversationsPlugin.nativeDebug = enableNative
        if enableSdk {
            TwilioConversationsClient.setLogLevel(TCHLogLevel.debug)
        } else {
            TwilioConversationsClient.setLogLevel(TCHLogLevel.warning)
        }
    }
    
    // Naming a by product of pigeon generation. This creates a conversations client, not a JWT token.
    func create(jwtToken: String, properties: PropertiesData, completion: @escaping (Result<ConversationClientData, Error>) -> Void) {
        debug("create => jwtToken: \(jwtToken)")
        
        let clientProperties = TwilioConversationsClientProperties()
        clientProperties.region = properties.region ?? "us1"
        
        SwiftTwilioConversationsPlugin.clientListener = ClientListener()
        
        TwilioConversationsClient.conversationsClient(
            withToken: jwtToken,
            properties: clientProperties,
            delegate: SwiftTwilioConversationsPlugin.clientListener,
            completion: { (result: TCHResult, conversationsClient: TwilioConversationsClient?) in
                if result.isSuccessful {
                    let myIdentity = conversationsClient?.user?.identity ?? "unknown"
                    self.debug("create => onSuccess - myIdentity: '\(myIdentity)'")
                    conversationsClient?.delegate = SwiftTwilioConversationsPlugin.clientListener
                    SwiftTwilioConversationsPlugin.instance?.client = conversationsClient
                    let clientData = Mapper.conversationsClientToPigeon(conversationsClient)
                    completion(Result.success(clientData!))
                } else {
                    self.debug("create => onError: \(String(describing: result.error))")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error creating client, Error: "
                                + "\(result.error.debugDescription)",
                                details: nil)))
                }
            })
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
