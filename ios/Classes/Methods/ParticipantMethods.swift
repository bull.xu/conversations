import Flutter
import TwilioConversationsClient

class ParticipantMethods: NSObject, ParticipantApi {
    let TAG = "ParticipantMethods"
    
    public func getUser(conversationSid: String, participantSid: String, completion: @escaping (Result<UserData, Error>) -> Void) {
        debug("getUser => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, participantSid: participantSid, completion: completion) { _, __, participant in
            participant.subscribedUser { result, user in
                if result.isSuccessful {
                    completion(Result.success(Mapper.userToPigeon(user)!))
                } else {
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "NotFoundException",
                                message: "No participant found with sid: \(participantSid)",
                                details: nil)))
                }
            }
        }
    }
    
    /// setAttributes
    public func setAttributes(conversationSid: String, participantSid: String, attributes: AttributesData, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setAttributes => conversationSid: \(String(describing: conversationSid))")
        
        var participantAttributes: TCHJsonAttributes?
        do {
            participantAttributes = try Mapper.pigeonToAttributes(attributes)
        } catch LocalizedConversionError.invalidData {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "Could not convert \(String(describing: attributes.data)) to valid TCHJsonAttributes",
                        details: nil)))
        } catch {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "\(String(describing: attributes.type)) is not a valid type for TCHJsonAttributes.",
                        details: nil)))
        }
        
        perform(conversationSid, participantSid: participantSid, completion: completion) { _, __, participant in
            participant.setAttributes(participantAttributes) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setAttributes => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setAttributes => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting attributes "
                                + "for participant \(participantSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// remove
    public func remove(conversationSid: String, participantSid: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("remove => conversationSid: \(String(describing: conversationSid)), "
              + "participantSid: \(String(describing: participantSid))")
        perform(conversationSid, participantSid: participantSid, completion: completion) { _, __, participant in
            participant.remove { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("remove => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("remove => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error removing participant \(participantSid) "
                                + "from conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
    
    
    /// A private method to ensure [TwilioConversationsClient] is ready, and
    /// connect to [conversation] with [conversationSid].
    private func perform<S>(
        _ conversationSid: String,
        participantSid: String,
        completion: @escaping (Result<S, Error>) -> Void,
        action: @escaping (TwilioConversationsClient, TCHConversation, TCHParticipant) -> Void
    ) {
        
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        
        client.conversation(withSidOrUniqueName: conversationSid) { (result, conversation) in
            
            if result.isSuccessful, let conversation = conversation {
                
                guard let participant = conversation.participant(withSid: participantSid) else {
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "NotFoundException",
                                message: "No participant found with sid: \(participantSid)",
                                details: nil)))
                    return
                }
                
                action(client, conversation, participant)
                
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("perform => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating conversation with sid \(conversationSid): \(errorMessage)",
                            details: nil)))
            }
        }
    }
}
