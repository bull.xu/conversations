import Flutter
import TwilioConversationsClient

// swiftlint:disable type_body_length
class MessageMethods: NSObject, MessageApi {
    let TAG = "MessageMethods"

    /// A private method to ensure [TwilioConversationsClient] is ready, and
    /// connect to [conversation] with [conversationSid].
    private func perform<S>(
        _ conversationSid: String,
        messageIndex: Int64,
        completion: @escaping (Result<S, Error>) -> Void,
        action: @escaping (TwilioConversationsClient, TCHConversation, TCHMessage) -> Void
    ) {
        
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        
        client.conversation(withSidOrUniqueName: conversationSid) { (result, conversation) in
            
            if result.isSuccessful, let conversation = conversation {
                conversation.message(withIndex: NSNumber(value: messageIndex)) { result, message in
                    if result.isSuccessful, let message = message {
                        action(client, conversation, message)
                    } else {
                        self.debug("perform => onError: \(String(describing: result.error))")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "NotFoundException",
                                    message: "Error locating message with index \(messageIndex) "
                                    + "in conversation \(conversationSid)",
                                    details: nil)))
                        
                    }
                }
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("perform => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating conversation with sid \(conversationSid): \(errorMessage)",
                            details: nil)))
            }
        }
    }
    
    public func getTemporaryContentUrlsForMedia(conversationSid: String, messageIndex: Int64, completion: @escaping (Result<[String : String], Error>) -> Void) {
        debug("getMediaContentTemporaryUrl => conversationSid: \(String(describing: conversationSid)), "
              + "messageIndex: \(String(describing: messageIndex))")
        perform(conversationSid, messageIndex: messageIndex, completion: completion) { _, __, message in
            message.getTemporaryContentUrlsForAttachedMedia { (result: TCHResult, urls: [String: URL]?) in
                if result.isSuccessful, let urls = urls {
                    self.debug("getMediaContentTemporaryUrl => onSuccess: \(urls)")
                    let mapped = urls.reduce(into: [String: String]()) { previous, url in
                        previous[url.key] = url.value.absoluteString
                    }
                    completion(Result.success(mapped))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("getMediaContentTemporaryUrl => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error getting mediaContentTemporaryUrl: "
                                + "\(errorMessage)",
                                details: nil)))
                }
            }
        }
    }

    /// getParticipant
    public func getParticipant(conversationSid: String, messageIndex: Int64, completion: @escaping (Result<ParticipantData, Error>) -> Void) {
        debug("getParticipant => conversationSid: \(String(describing: conversationSid)), "
              + "messageIndex: \(String(describing: messageIndex))")
        perform(conversationSid, messageIndex: messageIndex, completion: completion) { _, __, message in
            guard let participant = message.participant else {
                return completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Participant not found for message: \(messageIndex).",
                            details: nil)))
            }
            
            self.debug("getParticipant => onSuccess")
            let participantData =
            Mapper.participantToPigeon(participant, conversationSid: conversationSid)
            return completion(Result.success(participantData!))
        }
    }

    /// setAttributes
    public func setAttributes(conversationSid: String, messageIndex: Int64, attributes: AttributesData, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setAttributes => conversationSid: \(String(describing: conversationSid)), "
              + "messageIndex: \(String(describing: messageIndex))")
        
        var messageAttributes: TCHJsonAttributes?
        do {
            messageAttributes = try Mapper.pigeonToAttributes(attributes)
        } catch LocalizedConversionError.invalidData {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "Could not convert \(String(describing: attributes.data)) to valid TCHJsonAttributes",
                        details: nil)))
        } catch {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "\(String(describing: attributes.type)) is not a valid type for TCHJsonAttributes.",
                        details: nil)))
        }
        
        perform(conversationSid, messageIndex: messageIndex, completion: completion) { _, __, message in
            message.setAttributes(messageAttributes) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setAttributes => onSuccess")
                    return completion(Result.success(()))
                } else {
                    self.debug("setAttributes => onError: \(String(describing: result.error))")
                    return completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting attributes for message at "
                                + "index \(messageIndex) in conversation \(conversationSid)",
                                details: nil)))
                }
            }
        }
    }

    /// updateMessageBody
    public func updateMessageBody(conversationSid: String, messageIndex: Int64, messageBody: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("updateMessageBody => conversationSid: \(String(describing: conversationSid)), "
              + "messageIndex: \(String(describing: messageIndex))")
        perform(conversationSid, messageIndex: messageIndex, completion: completion) { _, __, message in
            message.updateBody(messageBody) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("updateMessageBody => onSuccess")
                    return completion(Result.success(()))
                } else {
                    self.debug("updateMessageBody => onError: \(String(describing: result.error))")
                    return completion(
                        Result.failure(
                        FlutterError(
                            code: "TwilioException",
                            message: "\(String(describing: result.error?.code))|Error updating message at index "
                            + "\(messageIndex) in conversation \(conversationSid)",
                            details: nil)))
                }
            }
        }
    }

    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
