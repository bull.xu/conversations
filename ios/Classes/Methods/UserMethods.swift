import Flutter
import TwilioConversationsClient

class UserMethods: NSObject, UserApi {
    
    let TAG = "UserMethods"
    
    /// setFriendlyName
    func setFriendlyName(identity: String, friendlyName: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setFriendlyName => identity: \(String(describing: identity))")
        perform(identity, completion: completion) { _, user in
            user.setFriendlyName(friendlyName) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setFriendlyName => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setFriendlyName => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting friendlyName \(friendlyName) for user "
                                + "\(identity): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// setAttributes
    func setAttributes(identity: String, attributes: AttributesData, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setAttributes => identity: \(String(describing: identity))")
        
        
        var userAttributes: TCHJsonAttributes?
        do {
            userAttributes = try Mapper.pigeonToAttributes(attributes)
        } catch LocalizedConversionError.invalidData {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "Could not convert \(String(describing: attributes.data)) to valid TCHJsonAttributes",
                        details: nil)))
        } catch {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "\(String(describing: attributes.type)) is not a valid type for TCHJsonAttributes.",
                        details: nil)))
        }
        
        
        perform(identity, completion: completion) { _, user in
            user.setAttributes(userAttributes) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setAttributes => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setAttributes => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting attributes \(String(describing: userAttributes)) for user "
                                + "\(identity): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
    
    /// A private method to ensure [TwilioConversationsClient] is ready, and
    /// connect to [conversation] with [conversationSid].
    private func perform<S>(
        _ identity: String,
        completion: @escaping (Result<S, Error>) -> Void,
        action: @escaping (TwilioConversationsClient, TCHUser) -> Void
    ) {
        
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        client.subscribedUser(withIdentity: identity) { (result: TCHResult, user: TCHUser?) in
            if result.isSuccessful, let user = user {
                action(client, user)
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("setFriendlyName => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating user \(identity): \(errorMessage)",
                            details: nil)))
            }
        }
    }
}
