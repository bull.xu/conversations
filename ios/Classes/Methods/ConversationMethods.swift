// swiftlint:disable file_length type_body_length
import Flutter
import TwilioConversationsClient

class ConversationMethods: NSObject, ConversationApi {
    let TAG = "ConversationMethods"
    
    /// A private method to ensure [TwilioConversationsClient] is ready, and
    /// connect to [conversation] with [conversationSid].
    private func perform<S>(
        _ conversationSid: String,
        completion: @escaping (Result<S, Error>) -> Void,
        action: @escaping (TwilioConversationsClient, TCHConversation) -> Void
    ) {
        
        guard let client = SwiftTwilioConversationsPlugin.instance?.client else {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ClientNotInitializedException",
                        message: "Client has not been initialized.",
                        details: nil)))
        }
        
        
        client.conversation(withSidOrUniqueName: conversationSid) { (result, conversation) in
            
            if result.isSuccessful, let conversation = conversation {
                action(client, conversation)
            } else {
                let errorMessage = String(describing: result.error)
                self.debug("perform => onError: \(errorMessage)")
                completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating conversation with sid \(conversationSid): \(errorMessage)",
                            details: nil)))
            }
        }
    }
    
    
    // swiftlint:disable function_body_length
    /// joinConversation
    public func join(conversationSid: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("join => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.join { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("join => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("join => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error joining "
                                + "conversation with sid \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// leaveConversation
    public func leave(conversationSid: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("leave => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.leave { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("leave => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("leave => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error leaving conversation \(conversationSid): "
                                + "\(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// destroyConversation
    public func destroy(conversationSid: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("destroy => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.destroy(completion: { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("destroy => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("destroy => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error destroying conversation \(conversationSid): "
                                + "\(errorMessage)",
                                details: nil)))
                }
            })
        }
    }
    
    /// typing
    public func typing(conversationSid: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("typing => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("typing => onSuccess")
            conversation.typing()
            completion(Result.success(()))
        }
    }
    
    /// sendMessage
    func sendMessage(conversationSid: String, options: MessageOptionsData, completion: @escaping (Result<MessageData, Error>) -> Void) {
        debug("sendMessage => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            
            let builder = conversation.prepareMessage()
            if let body = options.body {
                builder.setBody(body)
            }
            
            if let input = options.inputPath {
                guard let mimeType = options.mimeType else {
                    return completion(
                        Result.failure(
                            FlutterError(
                                code: "MissingParameterException",
                                message: "Missing 'mimeType' in MessageOptions",
                                details: nil)))
                }
                
                if let inputStream = InputStream(fileAtPath: input) {
                    builder.addMedia(inputStream: inputStream, contentType: mimeType, filename: options.filename)
                } else {
                    return completion(
                        Result.failure(
                            FlutterError(
                                code: "NotFoundException",
                                message: "Error locating file for upload from `\(input)`",
                                details: nil)))
                }
            }
            
            builder.buildAndSend { (result: TCHResult, message: TCHMessage?) in
                if result.isSuccessful,
                   let message = message {
                    self.debug("sendMessage => onSuccess")
                    completion(Result.success(Mapper.messageToPigeon(message, conversationSid: conversationSid)))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("sendMessage => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error sending message in conversation "
                                + "\(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// addParticipantByIdentity
    public func addParticipantByIdentity(conversationSid: String, identity: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        debug("addParticipantByIdentity => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.addParticipant(byIdentity: identity,
                                        attributes: nil) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("addParticipantByIdentity => onSuccess")
                    completion(Result.success(true))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("addParticipantByIdentity => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error adding participant to conversation "
                                + "\(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// removeParticipant
    public func removeParticipant(conversationSid: String, participantSid: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        debug("removeParticipant => conversationSid: \(conversationSid) participantSid: \(participantSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            guard let participant = conversation.participant(withSid: participantSid) else {
                return completion(
                    Result.failure(
                        FlutterError(
                            code: "NotFoundException",
                            message: "Error locating participant \(participantSid)",
                            details: nil)))
            }
            conversation.removeParticipant(participant) { (result: TCHResult) in
                if result.isSuccessful {
                    return completion(Result.success(true))
                } else {
                    return completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error removing participant \(participantSid)",
                                details: nil)))
                }
            }
        }
    }
    
    /// removeParticipantByIdentity
    public func removeParticipantByIdentity(conversationSid: String, identity: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        debug("removeParticipantByIdentity => conversationSid: \(conversationSid) identity: \(identity)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.removeParticipant(byIdentity: identity) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("removeParticipantByIdentity => onSuccess")
                    completion(Result.success(true))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("removeParticipantByIdentity => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error removing participant from "
                                + "conversation \(conversationSid): Error: \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// getParticipantByIdentity
    public func getParticipantByIdentity(conversationSid: String, identity: String, completion: @escaping (Result<ParticipantData, Error>) -> Void) {
        debug("getParticipantByIdentity => conversationSid: \(conversationSid) identity: \(identity)")
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("getParticipantByIdentity => onSuccess")
            guard let participant = conversation.participant(withIdentity: identity) else {
                completion(Result.failure(FlutterError(
                    code: "NotFoundException",
                    message: "No participant found with identity \(identity)",
                    details: nil)))
                return
            }
            let participantData = Mapper.participantToPigeon(participant, conversationSid: conversationSid)
            completion(Result.success(participantData!))
        }
    }
    
    /// getParticipantBySid
    public func getParticipantBySid(conversationSid: String, participantSid: String, completion: @escaping (Result<ParticipantData, Error>) -> Void) {
        debug("getParticipantBySid => conversationSid: \(conversationSid) participantSid: \(participantSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("getParticipantBySid => onSuccess")
            guard let participant = conversation.participant(withSid: participantSid) else {
                completion(Result.failure(FlutterError(
                    code: "NotFoundException",
                    message: "No participant found with sid \(participantSid)",
                    details: nil)))
                return
            }
            let participantData = Mapper.participantToPigeon(participant, conversationSid: conversationSid)
            completion(Result.success(participantData!))
        }
    }
    
    /// getParticipantsList
    public func getParticipantsList(conversationSid: String, completion: @escaping (Result<[ParticipantData], Error>) -> Void) {
        debug("getParticipantsList => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in                self.debug("getParticipantsList => onSuccess")
            let participantsList = conversation.participants().compactMap {
                Mapper.participantToPigeon($0, conversationSid: conversationSid)
            }
            completion(Result.success(participantsList))
        }
    }
    
    /// getMessagesCount
    public func getMessagesCount(conversationSid: String, completion: @escaping (Result<MessageCount, Error>) -> Void) {
        debug("getMessagesCount => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getMessagesCount(completion: { (result: TCHResult, count: UInt) in
                if result.isSuccessful {
                    self.debug("getMessagesCount => onSuccess")
                    var result = MessageCount()
                    result.count = Int64(count)
                    completion(Result.success(result))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("getMessagesCount => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error getting message count "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            })
        }
    }
    
    /// getUnreadMessagesCount
    public func getUnreadMessagesCount(conversationSid: String, completion: @escaping (Result<Int64, Error>) -> Void) {
        debug("getUnreadMessagesCount => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getUnreadMessagesCount { (result: TCHResult, count: NSNumber?) in
                if result.isSuccessful {
                    self.debug("getUnreadMessagesCount => onSuccess: \(String(describing: count))")
                    completion(Result.success(count?.int64Value ?? 0))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("getUnreadMessagesCount => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error retrieving unread messages count "
                                + "for conversation \(conversationSid): Error: \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// advanceLastReadMessageIndex
    public func advanceLastReadMessageIndex(conversationSid: String, lastReadMessageIndex: Int64, completion: @escaping (Result<MessageCount, Error>) -> Void) {
        debug("advanceLastReadMessageIndex => conversationSid: \(conversationSid) index: \(lastReadMessageIndex)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.advanceLastReadMessageIndex(
                NSNumber(value: lastReadMessageIndex),
                completion: { (result: TCHResult, count: UInt) in
                    if result.isSuccessful {
                        self.debug("advanceLastReadMessageIndex => onSuccess")
                        var result = MessageCount()
                        result.count = Int64(count)
                        completion(Result.success(result))
                    } else {
                        let errorMessage = String(describing: result.error)
                        self.debug("advanceLastReadMessageIndex => onError: \(errorMessage))")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "TwilioException",
                                    message: "\(String(describing: result.error?.code))|Error advancing last consumed message index "
                                    + "\(lastReadMessageIndex) for conversation \(conversationSid): \(errorMessage)",
                                    details: nil)))
                    }
                })
        }
    }
    
    /// setLastReadMessageIndex
    public func setLastReadMessageIndex(conversationSid: String, lastReadMessageIndex: Int64, completion: @escaping (Result<MessageCount, Error>) -> Void) {
        debug("setLastReadMessageIndex => conversationSid: \(conversationSid) index: \(lastReadMessageIndex)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.setLastReadMessageIndex(
                NSNumber(value: lastReadMessageIndex),
                completion: { (result: TCHResult, count: UInt) in
                    if result.isSuccessful {
                        self.debug("setLastReadMessageIndex => onSuccess")
                        var result = MessageCount()
                        result.count = Int64(count)
                        completion(Result.success(result))
                    } else {
                        let errorMessage = String(describing: result.error)
                        self.debug("setLastReadMessageIndex => onError: \(errorMessage))")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "TwilioException",
                                    message: "\(String(describing: result.error?.code))|Error setting last consumed message index "
                                    + "\(lastReadMessageIndex) for conversation \(conversationSid): \(errorMessage)",
                                    details: nil)))
                    }
                })
        }
    }
    
    /// setAllMessagesRead
    public func setAllMessagesRead(conversationSid: String, completion: @escaping (Result<MessageCount, Error>) -> Void) {
        debug("setAllMessagesRead => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.setAllMessagesReadWithCompletion({ (result: TCHResult, count: UInt) in
                if result.isSuccessful {
                    self.debug("setAllMessagesRead => onSuccess")
                    var result = MessageCount()
                    result.count = Int64(count)
                    completion(Result.success(result))
                } else {
                    self.debug("setAllMessagesRead => onError: \(String(describing: result.error))")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting all messages read for conversation "
                                + "\(conversationSid)",
                                details: nil)))
                }
            })
        }
    }
    
    /// setAllMessagesUnread
    public func setAllMessagesUnread(conversationSid: String, completion: @escaping (Result<MessageCount, Error>) -> Void) {
        debug("setAllMessagesUnread => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.setAllMessagesUnreadWithCompletion({ (result: TCHResult, count: NSNumber?) in
                if result.isSuccessful {
                    self.debug("setAllMessagesUnread => onSuccess")
                    var result = MessageCount()
                    result.count = count?.int64Value ?? 0
                    completion(Result.success(result))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setAllMessagesUnread => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting all messages read for conversation " +
                                "\(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            })
        }
    }
    
    /// getParticipantsCount
    public func getParticipantsCount(conversationSid: String, completion: @escaping (Result<Int64, Error>) -> Void) {
        debug("getParticipantsCount => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getParticipantsCount(completion: { (result: TCHResult, count: UInt) in
                if result.isSuccessful {
                    self.debug("getParticipantsCount => onSuccess")
                    let result = Int64(count)
                    completion(Result.success(result))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("getParticipantsCount => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error getting message count "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            })
        }
    }
    
    /// removeMessage
    public func removeMessage(conversationSid: String, messageIndex: Int64, completion: @escaping (Result<Bool, Error>) -> Void) {
        debug("removeMessage => conversationSid: \(conversationSid) index: \(messageIndex)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.message(withIndex: NSNumber(value: messageIndex)) { (_ result: TCHResult, message: TCHMessage?) in
                if result.isSuccessful, let message = message {
                    conversation.remove(message) { (result: TCHResult) in
                        if result.isSuccessful {
                            completion(Result.success(true))
                        } else {
                            let errorMessage = String(describing: result.error)
                            self.debug("removeMessage => onError: \(errorMessage)")
                            
                            completion(
                                Result.failure(
                                    FlutterError(
                                        code: "TwilioException",
                                        message: "\(String(describing: result.error?.code))|Error removing message \(String(describing: messageIndex)): "
                                        + "\(errorMessage)",
                                        details: nil)))
                        }
                    }
                }
            }
        }
    }
    
    /// getMessagesAfter
    public func getMessagesAfter(conversationSid: String, index: Int64, count: Int64, completion: @escaping (Result<[MessageData], Error>) -> Void) {
        debug("getMessagesAfter => conversationSid: \(conversationSid) index: \(index) count: \(count)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getMessagesAfter(
                UInt(index),
                withCount: UInt(count),
                completion: { (result: TCHResult, messages: [TCHMessage]?) in
                    if result.isSuccessful, let messages = messages {
                        self.debug("getMessagesAfter => onSuccess")
                        let messagesMap = messages.map { message in
                            Mapper.messageToPigeon(message, conversationSid: conversationSid)
                        }
                        completion(Result.success(messagesMap))
                    } else {
                        let errorMessage = String(describing: result.error)
                        self.debug("getMessagesAfter => onError: \(errorMessage)")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "TwilioException",
                                    message: "\(String(describing: result.error?.code))|Error retrieving \(count) messages before " +
                                    "message index: \(index) from conversation \(conversationSid): " +
                                    "\(errorMessage)",
                                    details: nil)))
                    }
                })
        }
    }
    
    /// getMessagesBefore
    public func getMessagesBefore(conversationSid: String, index: Int64, count: Int64, completion: @escaping (Result<[MessageData], Error>) -> Void) {
        debug("getMessagesBefore => conversationSid: \(conversationSid)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getMessagesBefore(
                UInt(index),
                withCount: UInt(count),
                completion: { (result: TCHResult, messages: [TCHMessage]?) in
                    if result.isSuccessful, let messages = messages {
                        self.debug("getMessagesBefore => onSuccess")
                        let messagesMap = messages.map { message in
                            Mapper.messageToPigeon(message, conversationSid: conversationSid)
                        }
                        completion(Result.success(messagesMap))
                    } else {
                        let errorMessage = String(describing: result.error)
                        self.debug("getMessagesBefore => onError: \(errorMessage)")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "TwilioException",
                                    message: "\(String(describing: result.error?.code))|Error retrieving \(count) messages before " +
                                    "message index: \(index) from conversation \(conversationSid)" +
                                    "\(errorMessage)",
                                    details: nil)))
                    }
                })
        }
    }
    
    /// getMessageByIndex
    public func getMessageByIndex(conversationSid: String, messageIndex: Int64, completion: @escaping (Result<MessageData, Error>) -> Void) {
        debug("getMessageByIndex => conversationSid: \(conversationSid) index: \(messageIndex)")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.message(withIndex: NSNumber(value: messageIndex)) { (_ result: TCHResult, message: TCHMessage?) in
                if result.isSuccessful, let message = message {
                    completion(
                        Result.success(Mapper.messageToPigeon(
                            message,
                            conversationSid: conversationSid)))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("getMessageByIndex => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error retrieving message \(messageIndex) " +
                                "in conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// getLastMessages
    public func getLastMessages(conversationSid: String, count: Int64, completion: @escaping (Result<[MessageData], Error>) -> Void) {
        debug("getLastMessages => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.getLastMessages(
                withCount: UInt(count),
                completion: { (result: TCHResult, messages: [TCHMessage]?) in
                    if result.isSuccessful, let messages = messages {
                        self.debug("getLastMessages => onSuccess")
                        let messagesMap = messages.map { message in
                            Mapper.messageToPigeon(message, conversationSid: conversationSid)
                        }
                        completion(Result.success(messagesMap))
                    } else {
                        self.debug("getLastMessages => onError: \(String(describing: result.error))")
                        completion(
                            Result.failure(
                                FlutterError(
                                    code: "TwilioException",
                                    message: "\(String(describing: result.error?.code))|Error retrieving last \(count) "
                                    + "messages for conversation \(conversationSid)",
                                    details: nil)))
                    }
                })
        }
    }
    
    /// setAttributes
    public func setAttributes(conversationSid: String, attributes: AttributesData, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setAttributes => conversationSid: \(String(describing: conversationSid))")
        var conversationAttributes: TCHJsonAttributes?
        do {
            conversationAttributes = try Mapper.pigeonToAttributes(attributes)
        } catch LocalizedConversionError.invalidData {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "Could not convert \(String(describing: attributes.data)) to valid TCHJsonAttributes",
                        details: nil)))
        } catch {
            return completion(
                Result.failure(
                    FlutterError(
                        code: "ConversionException",
                        message: "\(String(describing: attributes.type)) is not a valid type for TCHJsonAttributes.",
                        details: nil)))
        }
        
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("setAttributes => onSuccess")
            conversation.setAttributes(conversationAttributes) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setAttributes => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setAttributes => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting attributes "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// setFriendlyName
    public func setFriendlyName(conversationSid: String, friendlyName: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setFriendlyName => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("setFriendlyName => onSuccess")
            conversation.setFriendlyName(friendlyName) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setFriendlyName => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setFriendlyName => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting friendly name "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// setNotificationLevel
    public func setNotificationLevel(conversationSid: String, notificationLevel: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setNotificationLevel => conversationSid: "
              + "\(String(describing: conversationSid)) notificationLevel: \(String(describing: notificationLevel))")
        perform(conversationSid, completion: completion) { _, conversation in
            conversation.setNotificationLevel(Mapper.stringToNotificationLevel(notificationLevel)!) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setNotificationLevel => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setNotificationLevel => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting notification level "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    /// setUniqueName
    public func setUniqueName(conversationSid: String, uniqueName: String, completion: @escaping (Result<Void, Error>) -> Void) {
        debug("setUniqueName => conversationSid: \(String(describing: conversationSid))")
        perform(conversationSid, completion: completion) { _, conversation in
            self.debug("setUniqueName => onSuccess")
            conversation.setUniqueName(uniqueName) { (result: TCHResult) in
                if result.isSuccessful {
                    self.debug("setUniqueName => onSuccess")
                    completion(Result.success(()))
                } else {
                    let errorMessage = String(describing: result.error)
                    self.debug("setUniqueName => onError: \(errorMessage)")
                    completion(
                        Result.failure(
                            FlutterError(
                                code: "TwilioException",
                                message: "\(String(describing: result.error?.code))|Error setting unique name "
                                + "for conversation \(conversationSid): \(errorMessage)",
                                details: nil)))
                }
            }
        }
    }
    
    private func debug(_ msg: String) {
        SwiftTwilioConversationsPlugin.debug("\(TAG)::\(msg)")
    }
}
