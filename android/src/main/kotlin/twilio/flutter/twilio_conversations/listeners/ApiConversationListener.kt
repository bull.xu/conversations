package twilio.flutter.twilio_conversations.listeners

import com.twilio.conversations.Conversation
import com.twilio.conversations.ConversationListener
import com.twilio.conversations.Message
import com.twilio.conversations.Participant
import twilio.flutter.twilio_conversations.Mapper
import twilio.flutter.twilio_conversations.TwilioConversationsPlugin


class ApiConversationListener(private val conversationSid: String) : ConversationListener {
    private val TAG = "ConversationListener"

    override fun onMessageAdded(message: Message) {
        debug("onMessageAdded => messageSid = ${message.sid}")
        TwilioConversationsPlugin.flutterClientApi.messageAdded(
            conversationSid,
            Mapper.messageToPigeon(message)
        ) {}
    }

    override fun onMessageUpdated(message: Message, reason: Message.UpdateReason) {
        debug("onMessageUpdated => messageSid = ${message.sid}, reason = $reason")
        TwilioConversationsPlugin.flutterClientApi.messageUpdated(
            conversationSid,
            Mapper.messageToPigeon(message),
            reason.toString()
        ) {}
    }

    override fun onMessageDeleted(message: Message) {
        debug("onMessageDeleted => messageSid = ${message.sid}")
        TwilioConversationsPlugin.flutterClientApi.messageDeleted(
            conversationSid,
            Mapper.messageToPigeon(message)
        ) {}
    }

    override fun onParticipantAdded(participant: Participant) {
        debug("onParticipantAdded => participantSid = ${participant.sid}")
        Mapper.participantToPigeon(participant)?.let {
            TwilioConversationsPlugin.flutterClientApi.participantAdded(
                conversationSid,
                it
            ) {}
        }
    }

    override fun onParticipantUpdated(
        participant: Participant,
        reason: Participant.UpdateReason
    ) {
        debug("onParticipantUpdated => participantSid = ${participant.sid}, reason = $reason")
        Mapper.participantToPigeon(participant)?.let {
            TwilioConversationsPlugin.flutterClientApi.participantUpdated(
                conversationSid,
                it,
                reason.toString()
            ) {}
        }
    }

    override fun onParticipantDeleted(participant: Participant) {
        debug(".onParticipantDeleted => participantSid = ${participant.sid}")
        Mapper.participantToPigeon(participant)?.let {
            TwilioConversationsPlugin.flutterClientApi.participantDeleted(
                conversationSid,
                it
            ) {}
        }
    }

    override fun onTypingStarted(conversation: Conversation, participant: Participant) {
        debug("onTypingStarted => conversationSid = ${conversation.sid}, participantSid = ${conversation.sid}")
        Mapper.conversationToPigeon(conversation)?.let {
            Mapper.participantToPigeon(participant)?.let { it1 ->
                TwilioConversationsPlugin.flutterClientApi.typingStarted(
                    conversationSid,
                    it,
                    it1
                ) {}
            }
        }
    }

    override fun onTypingEnded(conversation: Conversation, participant: Participant) {
        debug("onTypingEnded => conversationSid = ${conversation.sid}, participantSid = ${participant.sid}")
        Mapper.conversationToPigeon(conversation)?.let {
            Mapper.participantToPigeon(participant)?.let { it1 ->
                TwilioConversationsPlugin.flutterClientApi.typingEnded(
                    conversationSid,
                    it,
                    it1
                ) {}
            }
        }
    }

    override fun onSynchronizationChanged(conversation: Conversation) {
        debug("onSynchronizationChanged => sid: ${conversation.sid}, status: ${conversation.synchronizationStatus}")
        Mapper.conversationToPigeon(conversation)?.let {
            TwilioConversationsPlugin.flutterClientApi.synchronizationChanged(
                conversationSid,
                it
            ) {}
        }
    }

    private fun debug(message: String) {
        TwilioConversationsPlugin.debug("$TAG::$message")
    }

}
